﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataSolutions.Katas
{
	public class Kata10ChooseYourOwnAdventure
	{
		public static string Simplify(string input)
		{
			string[] InputArray = input.Trim().Split("/");
			int numerator = int.Parse(InputArray[0]);
			int denominator = int.Parse(InputArray[1]);

			if (numerator % denominator == 0) return (numerator / denominator).ToString();

			if (denominator % numerator == 0)
			{
				return String.Join("/", (numerator / numerator).ToString(),
					(denominator / numerator).ToString());
			}

			for (int i = numerator / 2; i >= 2; i--)
			{
				if(numerator % i == 0 && denominator % i == 0)
				{
					return String.Join("/", (numerator / i).ToString(),
						(denominator / i).ToString());
				}
			}

			return input;
		}
	}
}
