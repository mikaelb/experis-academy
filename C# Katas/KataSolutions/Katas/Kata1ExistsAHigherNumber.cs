﻿namespace KataSolutions.Katas
{
	public class Kata1ExistsANumberHigher
	{
		public static bool ExistsHigher(int[] arr, int num)
		{
			foreach (int element in arr)
			{
				if (element >= num)
					return true;
			}
			return false;
		}
	}
}
