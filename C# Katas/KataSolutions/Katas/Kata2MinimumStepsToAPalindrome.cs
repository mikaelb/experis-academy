﻿namespace KataSolutions.Katas
{
	public class Kata2MinimumStepsToAPalindrome
	{
		public static int MinPalindromeSteps(string incompleteWord)
		{
			char[] chars = incompleteWord.ToCharArray();
			char[] reverseChars = incompleteWord.ToCharArray();
			Array.Reverse(reverseChars);

			return CheckForPalindrome(reverseChars, chars);

		}

		public static int CheckForPalindrome(char[] reverseChars, char[] chars)
		{
			string palindrome = new(reverseChars);
			string tempString = string.Empty;
			int counter = 0;
			for (int i = 0; i < chars.Length; i++)
			{
				tempString += chars[i];
				string possiblePalindrome = tempString + palindrome;
				char[] possiblePalindromeChars = possiblePalindrome.ToCharArray();
				Array.Reverse(possiblePalindromeChars);
				counter++;
				Console.WriteLine(possiblePalindrome);

				if (possiblePalindrome == new string(possiblePalindromeChars))
					break;
			}
			return counter;
		}
	}
}
