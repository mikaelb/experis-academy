﻿namespace KataSolutions.Katas
{
	public class Kata3ReplaceVowelWithACharacter
	{
		public static string ReplaceVowel(string input)
		{
			string replacedWord = string.Empty;
			input = input.ToLower();
			foreach (char c in input)
			{
				replacedWord += c switch
				{
					'a' => '1',
					'e' => '2',
					'i' => '3',
					'o' => '4',
					'u' => '5',
					_ => c,
				};
			}
			return replacedWord;
		}
	}
}
