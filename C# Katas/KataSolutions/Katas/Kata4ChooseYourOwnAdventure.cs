﻿namespace KataSolutions.Katas
{
	public class Kata4ChooseYourOwnAdventure
	{
		public static bool IsSmooth(string Sentence)
		{
			Sentence = Sentence.ToLower();
			string[] Words = Sentence.Split(' ');
			for (int i = 0; i < Words.Length - 1; i++)
			{
				if (Words[i][^1] != Words[i + 1][0])
				{
					return false;
				}
			}
			return true;
		}
	}
}
