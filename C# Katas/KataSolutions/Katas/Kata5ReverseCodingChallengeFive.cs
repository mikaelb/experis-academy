﻿namespace KataSolutions.Katas
{
	public class Kata5ReverseCodingChallengeFive
	{
		public static int SortByAscendingAndSubtract(int input)
		{
			int NumberAscended = int.Parse(String.Join("", input.ToString().ToCharArray().OrderBy(x => x)));
			if (input < NumberAscended)
				return 0;
			return input - NumberAscended;
		}
	}
}
