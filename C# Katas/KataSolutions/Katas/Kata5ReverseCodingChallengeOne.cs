﻿using System.Text;

namespace KataSolutions.Katas
{
	public class Kata5ReverseCodingChallengeOne
	{
		public static string ConvertStringTimesNumToString(string StringTimesNum)
		{
			StringBuilder ReturnString = new();
			for (int i = 0; i < StringTimesNum.Length; i++)
			{
				if (char.IsDigit(StringTimesNum[i]))
				{
					ReturnString.Append(StringTimesNum[i - 1], (int)char.GetNumericValue(StringTimesNum[i]));
				}
			}
			return ReturnString.ToString();
		}

	}
}
