﻿namespace KataSolutions.Katas
{
	public class Kata6PowerRanger
	{
		/// <summary>
		/// Takes in n, a, b and returns the number of positive values raised to the nth power that lie in the range [a, b], inclusive.
		/// </summary>
		/// <param name="power"></param>
		/// <param name="min"></param>
		/// <param name="max"></param>
		/// <returns>Integer of positive values raised to the nth power within the inclusive range.</returns>
		public static int PowerRangerWithLoop(int power, int min, int max)
		{
			int number = 0;
			int counter = 0;
			while (number < max / 2)
			{
				number++;
				if (min <= Math.Pow(number, power) && Math.Pow(number, power) <= max)
					counter++;
			}
			return counter;
		}
		public static int PowerRangerWithoutLoop(int power, int min, int max)
		{
			int LowRoot = (int)Math.Ceiling(Math.Pow(min, 1.0 / power));
			int HighRoot = (int)Math.Floor(Math.Pow(max, 1.0 / power));
			return HighRoot - LowRoot + 1;
		}
	}
}
