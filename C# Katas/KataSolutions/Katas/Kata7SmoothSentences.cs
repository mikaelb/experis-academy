﻿namespace KataSolutions.Katas
{
	public class Kata7SmoothSentences
	{
		public static bool IsSmooth(string input)
		{
			string[] Words = input.Trim().ToLower().Split(" ");
			if (Words.Length <= 1) return false;

			for (int i = 0; i < Words.Length - 1; i++)
			{
				if (Words[i][^1] != Words[i + 1][0]) return false;
			}
			return true;
		}
	}
}
