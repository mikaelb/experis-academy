﻿namespace KataSolutions.Katas
{
	public class Kata8SumOfFactorsOfFactors
	{
		public static int SumFF(int input)
		{
			if (input < 1) return 0;
			List<int> Factors = FindFactors(input);
			List<List<int>> FactorOfFactor = new();


			foreach (int factor in Factors)
			{
				FactorOfFactor.Add(FindFactors(factor));
			}

			int sum = 0;
			foreach (List<int> factor in FactorOfFactor)
			{
				sum += factor.Sum();
			}
			return sum;
		}

		public static List<int> FindFactors(int input)
		{
			List<int> factors = new();
			for (int i = 2; i <= input / 2; i++)
			{
				if (input % i == 0 && !factors.Contains(i))
				{
					factors.Add(i);
					if (!factors.Contains(input / i)) factors.Add(input / i);
				}
			}
			if (factors.Count > 0) factors.Add(0);
			return factors;
		}
	}
}
