﻿namespace KataSolutions.Katas
{
	// This is a reverse-coding challenge. Create a function that outputs the correct array from the input. Use the following examples to crack the code.
	public class Kata9ChooseYourOwnAdventure
	{
		public static int[] Decode(string input)
		{
			string LETTERS = "abcdefghijklmnopqrstuvwxyz";
			int[] decoded = new int[input.Length];

			for (int i = 0; i < input.Length; i++)
			{
				int index = LETTERS.IndexOf(input[i]) + 1;
				if (index > 13)
					decoded[i] = index - 12;
				else if (index == 0)
					decoded[i] = 5;
				else if (index - 3 <= 0)
					decoded[i] = index + 15;
				else
					decoded[i] = index - 3;
			}
			return decoded;
		}
	}
}
