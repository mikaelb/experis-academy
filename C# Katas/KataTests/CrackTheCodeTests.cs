﻿using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class CrackTheCodeTests
	{
		[Theory]
		[InlineData("hello", new int[] { 5, 2, 9, 9, 3 })]
		[InlineData("wonderful", new int[] { 11, 3, 2, 1, 2, 6, 3, 9, 9 })]
		[InlineData("something challenging", new int[] { 7, 3, 10, 2, 8, 5, 6, 2, 4, 5, 18, 5, 16, 9, 9, 2, 2, 4, 6, 2, 4 })]
		public void Decode_SendInString_GetLettersAsNumbersList(string input, int[] output)
		{
			// Arrange
			int[] Expected = output;
			// Act
			int[] actual = Kata9ChooseYourOwnAdventure.Decode(input);
			// Assert
			Assert.Equal(Expected, actual);
		}
	}
}
