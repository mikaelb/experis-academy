using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class ExistsANumberHigherTests
	{
		[Theory]
		[InlineData(new int[] { 5, 3, 15, 22, 4 }, 10, true)]
		[InlineData(new int[] { 1, 2, 3, 4, 5 }, 8, false)]
		[InlineData(new int[] { 4, 3, 3, 3, 2, 2, 2 }, 4, true)]
		[InlineData(new int[] { -10, -99, -57, -4 }, -4, true)]
		[InlineData(new int[] { 5, }, 5, true)]
		[InlineData(new int[] { 99, 99 }, 99, true)]
		[InlineData(new int[] { }, 5, false)]
		public void ExistsHigher_ChecksIfAnyNumberInListIsHigherThanCheckNumber_ReturnsCorrectBoolean(int[] input, int checkNumber, bool output)
		{
			// Arrange
			bool Expected = output;
			// Act
			bool Actual = Kata1ExistsANumberHigher.ExistsHigher(input, checkNumber);
			// Assert
			Assert.Equal(Expected, Actual);
		}
	}
}