﻿using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class MinimumStepsToAPalindromeTests
	{
		[Theory]
		[InlineData("race", 3)]
		[InlineData("mada", 1)]
		[InlineData("mirror", 3)]
		public void MinPalindromeSteps_WhenWordInserted_GetIntWithSteps(string input, int output)
		{
			// Arrange
			int Expected = output;
			// Act
			int Actual = Kata2MinimumStepsToAPalindrome.MinPalindromeSteps(input);
			// Assert
			Assert.Equal(Expected, Actual);
		}
	}
}
