﻿using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class PowerRangerTests
	{
		[Theory]
		[InlineData(2, 49, 65, 2)]
		[InlineData(3, 1, 27, 3)]
		[InlineData(10, 1, 5, 1)]
		[InlineData(5, 31, 33, 1)]
		[InlineData(4, 250, 1300, 3)]
		public void PowerRangerWithLoop_WhenInsertingThreeNumbers_ReturnsNumberOfPositiveValuesRaisedToNthPower(int n, int a, int b, int output)
		{
			// Arrange
			int Expected = output;
			// Act
			int Actual = Kata6PowerRanger.PowerRangerWithLoop(n, a, b);
			//Assert
			Assert.Equal(Expected, Actual);
		}

		[Theory]
		[InlineData(2, 49, 65, 2)]
		[InlineData(3, 1, 27, 3)]
		[InlineData(10, 1, 5, 1)]
		[InlineData(5, 31, 33, 1)]
		[InlineData(4, 250, 1300, 3)]
		public void PowerRangerWithOutLoop_WhenInsertingThreeNumbers_ReturnsNumberOfPositiveValuesRaisedToNthPower(int n, int a, int b, int output)
		{
			// Arrange
			int Expected = output;
			// Act
			int Actual = Kata6PowerRanger.PowerRangerWithoutLoop(n, a, b);
			//Assert
			Assert.Equal(Expected, Actual);
		}
	}
}
