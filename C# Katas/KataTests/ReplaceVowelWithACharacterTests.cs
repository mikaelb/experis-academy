﻿using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class ReplaceVowelWithACharacterTests
	{
		[Theory]
		[InlineData("karAchi", "k1r1ch3")]
		[InlineData("chEmBur", "ch2mb5r")]
		[InlineData("khandbari", "kh1ndb1r3")]
		[InlineData("LexiCAl", "l2x3c1l")]
		[InlineData("fuNctionS", "f5nct34ns")]
		[InlineData("EASY", "21sy")]
		public void ReplaceVowel_WhenStringIsSentIn_ReturnStringWithVowelReplaced(string input, string output)
		{
			// Arrange
			string Expected = output;
			// Act
			string Actual = Kata3ReplaceVowelWithACharacter.ReplaceVowel(input);
			// Assert
			Assert.Equal(Expected, Actual);
		}
	}
}
