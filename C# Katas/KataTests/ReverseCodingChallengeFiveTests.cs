﻿using KataSolutions.Katas;
using Xunit;


namespace KataTests
{
	public class ReverseCodingChallengeFiveTests
	{
		[Theory]
		[InlineData(832, 594)]
		[InlineData(51, 36)]
		[InlineData(7977, 198)]
		[InlineData(1, 0)]
		[InlineData(665, 99)]
		[InlineData(149, 0)]
		public void SortByAscendingAndSubtract_WhenIntInserted_GetIntMinusAscendingBack(int input, int output)
		{
			// Arrange
			int Expected = output;
			// Act
			int Actual = Kata5ReverseCodingChallengeFive.SortByAscendingAndSubtract(input);
			// Assert
			Assert.Equal(Expected, Actual);
		}
	}
}
