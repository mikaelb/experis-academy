using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class ReverseCodingChallengeOneTests
	{
		[Theory]
		[InlineData("A4B5C2", "AAAABBBBBCC")]
		[InlineData("C2F1E5", "CCFEEEEE")]
		[InlineData("T4S2V2", "TTTTSSVV")]
		[InlineData("A1B2C3D4", "ABBCCCDDDD")]
		public void ConvertStringTimesNumToString_WhenStringInserted_GetCorrectStringBack(string StringTimesNum, string AnswerString)
		{
			// Arrange
			Kata5ReverseCodingChallengeOne code = new();
			string Expected = AnswerString;
			// Act
			string Actual = Kata5ReverseCodingChallengeOne.ConvertStringTimesNumToString(StringTimesNum);
			// Assert
			Assert.Equal(Expected, Actual);
		}
	}
}