﻿using KataSolutions.Katas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace KataTests
{
	public class SimplifiedFractionsTests
	{
		[Theory]
		[InlineData("4/6", "2/3")]
		[InlineData("10/11", "10/11")]
		[InlineData("100/400", "1/4")]
		[InlineData("8/4", "2")]
		public void Simplify_WhenStringFractionIsSentIn_ReturnsFractionAsSimplified(string input, string output)
		{
			// Arrange
			string Expected = output;
			// Act
			string Actual = Kata10ChooseYourOwnAdventure.Simplify(input);
			// Assert
			Assert.Equal(Expected, Actual);
		}
	}
}
