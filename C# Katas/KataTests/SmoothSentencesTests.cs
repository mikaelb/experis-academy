﻿using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class SmoothSentencesTests
	{
		[Theory]
		[InlineData("Marta appreciated deep perpendicular right trapezoids", true)]
		[InlineData("Someone is outside the doorway", false)]
		[InlineData("She eats Super righteously", true)]
		public void IsSmooth_WhenStringInserted_ReturnsBool(string input, bool output)
		{
			// Arrange
			bool Expected = output;
			// Act
			bool Actual = Kata4ChooseYourOwnAdventure.IsSmooth(input);
			// Assert
			Assert.Equal(Expected, Actual);
		}

		[Theory]
		[InlineData("Marta appreciated deep perpendicular right trapezoids", true)]
		[InlineData("Someone is outside the doorway", false)]
		[InlineData("She eats Super righteously", true)]
		public void IsSmooth_WhenSentenceIsSentIn_ChecksIfSentenceIsSmooth(string input, bool output)
		{
			// Arrange
			bool Expected = output;
			// Act
			bool Actual = Kata7SmoothSentences.IsSmooth(input);
			// Assert
			Assert.Equal(Expected, Actual);
		}
	}
}
