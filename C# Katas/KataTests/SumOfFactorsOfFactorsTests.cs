﻿using KataSolutions.Katas;
using Xunit;

namespace KataTests
{
	public class SumOfFactorsOfFactorsTests
	{
		[Theory]
		[InlineData(69, 0)]
		[InlineData(12, 7)]
		[InlineData(420, 1175)]
		[InlineData(619, 0)]
		public void SumFF_WhenNumberIsSent_ReturnSumOfFactorsOfFactors(int input, int output)
		{
			// Arrange
			int Expected = output;
			// Act
			int actual = Kata8SumOfFactorsOfFactors.SumFF(input);
			// Assert
			Assert.Equal(Expected, actual);
		}
	}
}
