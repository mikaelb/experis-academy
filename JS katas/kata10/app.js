const ALPHABET = "abcdefghijklmnopqrstuvwxyz";

const caesarCipher = (message, rotation) => {
  let encryptedMessage = "";
  for (const char of message) {
    if (ALPHABET.includes(char)) {
      encryptedMessage += encryptChar(char, rotation);
    } else if (ALPHABET.includes(char.toLowerCase())) {
      encryptedMessage += encryptChar(
        char.toLowerCase(),
        rotation
      ).toUpperCase();
    } else {
      encryptedMessage += char;
    }
  }
  return encryptedMessage;
};

const encryptChar = (char, rotation) => {
  return ALPHABET[(ALPHABET.indexOf(char) + rotation) % ALPHABET.length];
};

console.time("Execution Time");
console.log(caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5));
console.timeEnd("Execution Time");
// "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"

console.time("Execution Time");
console.log(caesarCipher("A friend in need is a friend indeed", 20));
console.timeEnd("Execution Time");
// "U zlcyhx ch hyyx cm u zlcyhx chxyyx"
console.time("Execution Time");
console.log(caesarCipher("middle-Outz", 2)); // "okffng-Qwvb"
console.timeEnd("Execution Time");
