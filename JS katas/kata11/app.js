// Choose Your Own Adventure again
// Go on to https://edabit.com/ or any other similar website and choose your own problem to solve
// You can decide on difficulty
// If you want a real challenge then try
// https://projecteuler.net/

// There are three towers. The objective of the game is to move all the disks over to tower #3,
// but you can't place a larger disk onto a smaller disk.

// Create a function that takes a number discs as an argument and
// returns the minimum amount of steps needed to complete the game.
// The amount of discs is always a positive integer.
// 1 disc can be changed per move.

const solveTowerHanoi = (
    numberOfDiscs,
    stackOriginal,
    stackTemporary = { list: [], stack: "Stack two" },
    stackTarget = { list: [], stack: "Stack three" },
    counter = 0
) => {
    if (numberOfDiscs > 0) {
        counter = solveTowerHanoi(
            numberOfDiscs - 1,
            stackOriginal,
            stackTarget,
            stackTemporary,
            counter
        );
        if (stackOriginal.list) {
            disc = stackOriginal.list.pop();
            counter++;
            stackTarget.list.push(disc);
            // console.log(`${stackOriginal.stack}: ${stackOriginal.list}`);
            // console.log(`${stackTarget.stack}: ${stackTarget.list}`);
            // console.log(`${stackTemporary.stack}: ${stackTemporary.list}`);
        }
        counter = solveTowerHanoi(
            numberOfDiscs - 1,
            stackTemporary,
            stackOriginal,
            stackTarget,
            counter
        );
    }
    return counter;
};

const towerHanoi = (discs) => {
    if (discs <= 0) {
        return 0;
    }
    stack = Array(discs)
        .fill()
        .map((_, idx) => discs - idx);

    return solveTowerHanoi(discs, { list: stack, stack: "Stack one" });
};

console.log(towerHanoi(0)); // 0

console.log(towerHanoi(1));

console.log(towerHanoi(2));

console.log(towerHanoi(3)); // 7

console.log(towerHanoi(4));

console.log(towerHanoi(5)); // 31

console.log(towerHanoi(6));

console.log(towerHanoi(20));
