// Create a function that takes two strings.
// The first string contains a sentence containing
// the letters of the second string in a consecutive sequence
// but in a different order.
// The hidden anagram must contain all the letters,
// including duplicates, from the second string in any order
// and must not contain any other alphabetic characters.

// Write a function to find the anagram of the second string
// embedded somewhere in the first string.
// You should ignore character case,
// any spaces, and punctuation marks and
// return the anagram as a lower case string
// with no spaces or punctuation marks.

const hiddenAnagram = (firstString, secondString) => {
    firstString = firstString.toLowerCase().replace(/[^a-z]/g, "");
    secondString = secondString.toLowerCase().replace(/[^a-z]/g, "");
    for (
        let index = 0;
        index < firstString.length - secondString.length + 1;
        index++
    ) {
        const stringSlice = firstString.slice(
            index,
            index + secondString.length
        );
        if (checkSlice(stringSlice, secondString)) {
            return stringSlice;
        }
    }
    return "notfound";
};

const checkSlice = (stringSlice, secondString) => {
    let stringCheck = secondString;
    for (const char of stringSlice) {
        if (!stringCheck.includes(char)) {
            return false;
        }
        stringCheck =
            stringCheck.slice(0, stringCheck.indexOf(char)) +
            stringCheck.slice(stringCheck.indexOf(char) + 1);
    }
    return true;
};

console.log(hiddenAnagram("An old west action hero actor", "Clint Eastwood"));
// "noldwestactio"

console.log(
    hiddenAnagram("Mr. Mojo Rising could be a song title", "Jim Morrison")
); // "mrmojorisin"

console.log(hiddenAnagram("Banana? margaritas", "ANAGRAM"));
// "anamarg"

console.log(hiddenAnagram("D e b90it->?$ (c)a r...d,,#~", "bad credit"));
// "debitcard"

console.log(hiddenAnagram("Bright is the moon", "Bongo mirth"));
// "noutfond"
