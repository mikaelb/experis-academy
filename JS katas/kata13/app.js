// The sum of the squares of the first ten natural numbers is,
// 1^2+2^2+...+10^2 = 385

// The square of the sum of the first ten natural numbers is,
// (1+2+...+10)^2 = 55^2 = 3025

// Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is

// 3025 - 385 = 2640

// Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

const sumSquareDifferenceLoop = (num) => {
	let sumOfSquared = 0;
	let squareOfSum = 0;
	for (let index = 1; index < num + 1; index++) {
		sumOfSquared += index ** 2;
		squareOfSum += index;
	}
	return squareOfSum ** 2 - sumOfSquared;
};

const sumSquareDifference = (num) => {
	const sumOfSquared = (num * (num + 1) * (2 * num + 1)) / 6;
	const squareOfSum = (num * (num + 1)) / 2;
	return squareOfSum ** 2 - sumOfSquared;
};

console.log(sumSquareDifference(100));
console.log(sumSquareDifferenceLoop(100));

console.time("Execution Time");
console.log(sumSquareDifference(10 * 10 ** 8));
console.timeEnd("Execution Time");

console.time("Execution Time loop");
console.log(sumSquareDifferenceLoop(10 * 10 ** 8));
console.timeEnd("Execution Time loop");
