const nthPrimeNumber = (nth) => {
	let primeNumber = 0;
	let counter = 0;
	while (counter < nth) {
		primeNumber++;
		if (checkPrime(primeNumber)) {
			counter++;
		}
	}

	return primeNumber;
};

const checkPrime = (primeNumber) => {
	if (primeNumber === 2) return true;
	if (primeNumber <= 1 || primeNumber % 2 === 0) return false;
	for (let index = 3; index < primeNumber; index += 2) {
		if (primeNumber % index === 0 && primeNumber !== index) {
			return false;
		}
	}
	return true;
};

const nthPrimeNumberSqrt = (nth) => {
	let primeNumber = 0;
	let counter = 0;
	while (counter < nth) {
		if (primeNumber < 3) {
			primeNumber++;
		} else {
			primeNumber += 2;
		}

		if (checkPrimeSqrt(primeNumber)) {
			counter++;
		}
	}

	return primeNumber;
};

const checkPrimeSqrt = (primeNumber) => {
	// If 2 or 3, return true
	if (primeNumber === 2 || primeNumber === 3) return true;
	// If divisible by 2 or 3, return false
	if (primeNumber <= 1 || primeNumber % 2 === 0 || primeNumber % 3 === 0)
		return false;

	// Reduce loop limit to sqrt(primeNumber) + 1
	let step = 4;
	let maxCount = Math.sqrt(primeNumber) + 1;

	for (let index = 5; index < maxCount; index += step) {
		// If divisible by index, return false
		if (primeNumber % index === 0) {
			return false;
		}

		// Ensures that we do not check in the multiples of 3 and 5
		// 5, 7, 11, 13, 17, 19 etc...
		step = 6 - step;
	}

	return true;
};

console.log(nthPrimeNumber(1)); // 2
console.log(nthPrimeNumber(2)); // 3
console.log(nthPrimeNumber(3)); // 5
console.log(nthPrimeNumber(4)); // 7
console.log(nthPrimeNumber(5)); // 11
console.log(nthPrimeNumber(6)); // 13
console.time("Bruteforce");
console.log(nthPrimeNumber(10001));
console.timeEnd("Bruteforce");

console.time("Smarter bruteforce");
console.log(nthPrimeNumberSqrt(10001));
console.timeEnd("Smarter bruteforce");
