// Write a function called splitOnDoubleLetter()
// that receives a word, of type string, as an argument.
// The function should split the word 
// where any double letter is found and 
// return an array of the split word.
// If no repeated letters are found, 
// simply return an empty array


const splitOnDoubleLetter = (word) => {
    let splittedWord = [];
    let prevIndex = 0
    let doubleLetter = false;
    for (let i = 1; i < word.length; i++) {
        if (word[i - 1].toLowerCase() === word[i].toLowerCase()) {
            doubleLetter = true;
            splittedWord.push(word.slice(prevIndex, i));
            prevIndex = i
        }
    }
    if (doubleLetter === true) {
        splittedWord.push(word.slice(prevIndex));
    }
    return splittedWord;
}

console.log(splitOnDoubleLetter("Aardvark"));

console.log(splitOnDoubleLetter("Really"));

console.log(splitOnDoubleLetter("Happy"));

console.log(splitOnDoubleLetter("Shall"));

console.log(splitOnDoubleLetter("Tool"));

console.log(splitOnDoubleLetter("Mississippi"));

console.log(splitOnDoubleLetter("Easy"));