// https://projecteuler.net/problem=25
// The Fibonacci sequence is defined by the recurrence relation:
// Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
// Hence the first 12 terms will be:
// F1 = 1
// F2 = 1
// F3 = 2
// F4 = 3
// F5 = 5
// F6 = 8
// F7 = 13
// F8 = 21
// F9 = 34
// F10 = 55
// F11 = 89
// F12 = 144
// The 12th term, F12, is the first term to contain three digits.
// What is the index of the first term in the Fibonacci sequence to contain 1000 digits ?


const fibonacciThousandDigits = () => {
    let previousNumber = BigInt(1);
    let currentNumber = BigInt(1);
    let fiboNumber = BigInt(1);
    let counter = 2
    while (fiboNumber < 10**999) {
        fiboNumber = currentNumber + previousNumber;
        previousNumber = currentNumber;
        currentNumber = fiboNumber;
        counter++;
    }
    console.log("The first index with 1000 digits is: ", counter)
}

const betterThousandDigits = () => {
    let i = 0;
    let counter = 2;
    let limit = 10**999;
    let fib = [BigInt(1), BigInt(0), BigInt(1)];
    while (fib[i] < limit) {
        i = (i + 1) % 3;
        counter++;
        fib[i] = fib[(i + 1) % 3] + fib[(i + 2) % 3];
    }
    console.log("The first index with 1000 digits is: ", counter)
}

betterThousandDigits();
//fibonacciThousandDigits();

