// https://edabit.com/challenge/WCMC8X9z7758hq4gP
// Create a function that returns the thickness(in meters) of a piece of paper after folding it n number of times.
// The paper starts off with a thickness of 0.5mm.
    // There are 1000mm in a single meter.
    // Don't round answers.

const numLayers = (layers) => {
    const thickness = 0.0005;
    return thickness*2**layers
}


console.log(numLayers(1)) // ➞ "0.001m"
// Paper folded once is 1mm (equal to 0.001m)

console.log(numLayers(4)) // ➞ "0.008m"
// Paper folded 4 times is 8mm (equal to 0.008m)

console.log(numLayers(21)) // ➞ "1048.576m"
// Paper folded 21 times is 1048576mm (equal to 1048.576m)