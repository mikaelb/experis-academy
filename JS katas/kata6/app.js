// Write a function that takes a string and returns a string with the correct case for character titles in the Game of Thrones series.

// The words and, the, of and in should be lowercase.
// All other words should have the first character as uppercase and the rest lowercase.
// All commas must always be followed by a single space.
// All titles must end with a period.

// Punctuation and spaces must remain in their original positions.
// All commas must be followed by a single space.
// Titles must end with a period.
// Hyphenated words are considered separate words.
// Be careful with words that contain and, the, of or in.


const correctTitle = (sentence) => {
    sentence = sentence.toLowerCase().split(" ");
    badWords = ["and", "the", "of", "in"]
    for (let i = 0; i<sentence.length; i++) {
        if (sentence[i].includes(",") && sentence[i].charAt(sentence[i].length-1) !== ",") {
            let newArr = sentence[i].split(",")
            sentence.splice(i, 1)
            sentence.splice(i, 0, newArr[0].concat(","))
            sentence.splice(i+1, 0, newArr[1])
        }
        if (!badWords.includes(sentence[i])) {
            sentence[i] = sentence[i].charAt(0).toUpperCase() + sentence[i].slice(1)
        }
    }
    sentence = sentence.join(" ")
    if (sentence.charAt(sentence.length-1) !== ".") {
        sentence = sentence.concat(".")
    }
    console.log(sentence);
}

correctTitle("jOn SnoW, kINg IN thE noRth")
correctTitle("sansa stark,lady of winterfell.")
correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.")