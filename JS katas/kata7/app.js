// Write a function shuffleCount that takes 
// a positive even integer num representing 
// the number of the cards in a deck, and 
// returns the number of out - shuffles required to 
// return the deck to its original order.

const shuffleCount = (cardnum) => {
    if (cardnum > 1 && cardnum % 2 == 0) {
        const cards = [...Array(cardnum).keys()].map( i => i+1)
        let shuffeledCards = cards
        let keepGoing = true
        let counter = 0
        while (keepGoing) {
            shuffeledCards = shuffleCards(shuffeledCards)
            counter++
            if (compareArrays(cards, shuffeledCards)) {
                keepGoing = false
            }
        }
        return counter
    } else return "Deck too small or uneven!"
}

const shuffleCards = (cards) => {
    const half = cards.length/2
    const newDeck = cards.slice(0, half)
    let index = 1
    for (let i = half; i<cards.length; i++) {
        newDeck.splice(index, 0, cards[i])
        index += 2
    }
    return newDeck
}

const compareArrays = (array1, array2) => {
    const check = array1.every(function (element, index) {
        return element === array2[index]
    })
    return check
}

console.log(shuffleCount(8))