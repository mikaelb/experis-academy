

const countBoomerangs = (array) => {
    const boomerangs = []
    for (let index = 2; index < array.length; index++) {
        if (array[index - 2] === array[index] && array[index - 1] !== array[index]) {
            boomerangs.push([index - 2, index - 1, index])
        }
    }
    return boomerangs.length
}


const countBoomerangsCounter = (array) => {
    let count = 0
    for (let index = 2; index < array.length; index++) {
        if (array[index - 2] === array[index] && array[index - 1] !== array[index]) {
            count++
        }
    }
    return count

}

console.time('Execution Time')
console.log(countBoomerangs([9, 5, 9, 5, 1, 1, 1])) // 2
console.log(countBoomerangs([5, 6, 6, 7, 6, 3, 9])) // 1
console.log(countBoomerangs([4, 4, 4, 9, 9, 9, 9])) // 0
console.log(countBoomerangs([1, 7, 1, 7, 1, 7, 1])) // 5
console.timeEnd('Execution Time')
console.time('Execution Time')
console.log(countBoomerangsCounter([9, 5, 9, 5, 1, 1, 1])) // 2
console.log(countBoomerangsCounter([5, 6, 6, 7, 6, 3, 9])) // 1
console.log(countBoomerangsCounter([4, 4, 4, 9, 9, 9, 9])) // 0
console.log(countBoomerangsCounter([1, 7, 1, 7, 1, 7, 1])) // 5
console.timeEnd('Execution Time')