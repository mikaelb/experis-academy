const ticTacToe = (array) => {
  let check = checkRow(array);
  if (check) return check;

  array = array[0].map((_, colIndex) => array.map((row) => row[colIndex]));

  check = checkRow(array);
  if (check) return check;

  let diags = [
    [array[0][0], array[1][1], array[2][2]],
    [array[0][2], array[1][1], array[2][0]],
  ];

  check = checkRow(diags);
  if (check) return check;

  return "Draw";
};

const checkRow = (array) => {
  for (const row of array) {
    if (row.every((ele) => ele === row[0]) && row[0] !== "E") {
      return row[0];
    }
  }
  return null;
};

console.time("Execution Time");
console.log(
  ticTacToe([
    ["X", "O", "X"],

    ["O", "X", "O"],

    ["O", "X", "X"],
  ])
); // X
console.log(
  ticTacToe([
    ["O", "O", "O"],

    ["O", "X", "X"],

    ["E", "X", "X"],
  ])
); // O
console.log(
  ticTacToe([
    ["X", "X", "O"],

    ["O", "O", "X"],

    ["X", "X", "O"],
  ])
); // Draw
console.log(
  ticTacToe([
    ["X", "E", "O"],

    ["O", "O", "E"],

    ["O", "E", "X"],
  ])
); // O
console.log(
  ticTacToe([
    ["X", "E", "X"],

    ["O", "X", "E"],

    ["O", "E", "X"],
  ])
); // X
console.timeEnd("Execution Time");
